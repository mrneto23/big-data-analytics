---
title: "Análise exploratória das viagens"
author: "Moacyr Rodrigues Neto"
date: "8/6/2019"
output:
  pdf_document: default
  html_document: default
---

```{r setup, include=FALSE}
#knitr::opts_chunk$set(collapse = TRUE)
knitr::opts_chunk$set(echo = TRUE)
```

```{r include=FALSE}
# Carregando bibliotecas
library(tidyverse)
```

```{r include=FALSE}
# Carregando bases
passagens <- read_rds(path = "data/2019_passagens.rds")
viagens <- read_rds(path = "data/2019_viagens.rds")
pagamentos <- read_rds(path = "data/2019_pagamentos.rds")
trechos <- read_rds(path = "data/2019_trechos.rds")

#   Removi esse cidadão, pois aparentemente houve um erro no lançamento da sua viagem.
#   Mais de 7milhões em outros gastos e ao comparar com a base de pagamento vimos que foram gastos R$79,21.
#  filter(viajante_nome != "MURILO JOSE PERINI DA SILVA BRAGA") %>% 
viagens <- viagens %>%
  filter(viajante_nome != "MURILO JOSE PERINI DA SILVA BRAGA")
```


# Análise Exploratória

## Questionamentos sobre a base de dados

1 - Valor total gasto com viagens
```{r}
valorTotal <- sum(viagens$valor_diarias) + sum(viagens$valor_passagens) + sum(viagens$valor_outros_gastos)
cat("Valor total gasto com viagens: R$", valorTotal)
```


  1.1 - Realizadas
```{r}
viagens_realizadas <- viagens %>% 
  filter(situacao == "Realizada")
```

  1.1.1 - Passagem
```{r}
valorPassagens <- sum(viagens_realizadas$valor_passagens)
cat("Valor gasto com passagens: R$", valorPassagens)
```
    
  1.1.2 - Diárias
```{r}
valorDiarias <- sum(viagens_realizadas$valor_diarias)
cat("Valor gasto com diárias: R$", valorDiarias)
```
    
  1.1.3 - Outros
```{r}
valorOutros <- sum(viagens_realizadas$valor_outros_gastos)
cat("Valor gasto com outros: R$", valorOutros)
```
    

  1.2 - Não Realizadas
```{r}
viagens_nao_realizadas <- viagens %>% 
  filter(situacao == "Não realizada")
```

  1.2.1 - Passagem
```{r}
valorPassagens <- sum(viagens_nao_realizadas$valor_passagens)
cat("Valor gasto com passagens: R$", valorPassagens)
```
  
  1.2.2 - Diárias
```{r}
valorDiarias <- sum(viagens_nao_realizadas$valor_diarias)
cat("Valor gasto com diárias: R$", valorDiarias)
```
  
  1.2.3 - Outros
```{r}
valorOutros <- sum(viagens_nao_realizadas$valor_outros_gastos)
cat("Valor gasto com outros: R$", valorOutros)
```

2 - Orgão que mais gastou

```{r}
viagens %>% 
  group_by(nome_orgao_superior) %>% 
  summarise(Gastos_Totais = sum(valor_diarias) + sum(valor_passagens) + sum(valor_outros_gastos)) %>% 
  arrange(desc(Gastos_Totais)) %>% 
  head(10)
```


  2.1 - Realizadas
```{r}
viagens_realizadas %>% 
  group_by(nome_orgao_superior) %>% 
  summarise(Gastos_Totais = sum(valor_diarias) + sum(valor_passagens) + sum(valor_outros_gastos)) %>% 
  arrange(desc(Gastos_Totais)) %>% 
  head(10)
```
  

  2.1.1 - Passagem
```{r}
viagens_realizadas %>% 
  group_by(nome_orgao_superior) %>% 
  summarise(Gastos_Passagens = sum(valor_passagens)) %>% 
  arrange(desc(Gastos_Passagens)) %>% 
  head(10)
```
  
  
  2.1.2 - Diárias
```{r}
viagens_realizadas %>%  
  group_by(nome_orgao_superior) %>% 
  summarise(Gastos_Diarias = sum(valor_diarias)) %>% 
  arrange(desc(Gastos_Diarias)) %>% 
  head(10)
```
  
  
  2.1.3 - Outros
```{r}
viagens_realizadas %>% 
  group_by(nome_orgao_superior) %>% 
  summarise(Gastos_Outros = sum(valor_outros_gastos)) %>% 
  arrange(desc(Gastos_Outros)) %>% 
  head(10)
```
  

  2.2 - Não realizadas
```{r}
viagens_nao_realizadas %>% 
  group_by(nome_orgao_superior) %>% 
  summarise(Gastos_Totais = sum(valor_diarias) + sum(valor_outros_gastos) + sum(valor_passagens)) %>% 
  arrange(desc(Gastos_Totais)) %>% 
  head(10)
```
  

  2.2.1 - Passagem
```{r}
viagens_nao_realizadas %>% 
  group_by(nome_orgao_superior) %>% 
  summarise(Gastos_Passagens = sum(valor_passagens)) %>% 
  arrange(desc(Gastos_Passagens)) %>% 
  head(10)
```
  
  
  2.2.2 - Diárias
```{r}
viagens_nao_realizadas %>% 
  group_by(nome_orgao_superior) %>% 
  summarise(Gastos_Diarias = sum(valor_diarias)) %>% 
  arrange(desc(Gastos_Diarias)) %>% 
  head(10)
```
  
  
  2.2.3 - Outros
```{r}
viagens_nao_realizadas %>% 
  group_by(nome_orgao_superior) %>% 
  summarise(Gastos_Outros = sum(valor_outros_gastos)) %>% 
  arrange(desc(Gastos_Outros)) %>% 
  head(10)
```
  

3 - Orgão que mais viajou
```{r}
viagens %>% 
  group_by(nome_orgao_superior) %>% 
  summarise(Total_Viagens = length(unique(id_viagem))) %>% 
  arrange(desc(Total_Viagens)) %>% 
  head(10)
```


4 - Destinos mais requisitados



5 - Ranking das pessoas com os maiores gastados
```{r}
viagens %>% 
  group_by(viajante_nome) %>% 
  summarise(Viagens = length(unique(id_viagem)), Valor_Passagens = sum(valor_passagens),
            Valor_Diarias = sum(valor_diarias), Valor_Outros = sum(valor_outros_gastos),
            Valor_Total = Valor_Diarias + Valor_Passagens + Valor_Outros) %>% 
  arrange(desc(Valor_Total)) %>% 
  head(10)
```


6 - Pessoas com o maior número de viagens

Investigar melhor, pois a PATRICIA TIDORI MIURA, realizou o maior número de vagens todas com carro próprio, sem custos com passagens.
```{r}
viagens_realizadas %>% 
  group_by(viajante_nome) %>% 
  summarise(Viagens = length(unique(id_viagem)), Valor_Passagens = sum(valor_passagens),
            Valor_Diarias = sum(valor_diarias), Valor_Outros = sum(valor_outros_gastos),
            Valor_Total = Valor_Diarias + Valor_Passagens + Valor_Outros) %>% 
  arrange(desc(Viagens)) %>% 
  head(10)
```


















