---
title: "Avalie seus conhecimentos em R"
author: "Moacyr Rodrigues Neto"
date: "March 6, 2019"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r}
L = mtcars$cyl > 6 & mtcars$disp > 100; L

mtcars[L, c(1,2,3)]
```

```{r}
library(tidyverse)
```

```{r}
head(iris)
```

```{r}
summary(filter(iris, iris$Petal.Width == 0.2))
```

```{r}
summary(filter(iris, iris$Petal.Length >= 1.5 & iris$Petal.Length <=4.5))
```

```{r}
summary(filter(iris, iris$Petal.Length >= 1.5 & iris$Petal.Length <= 4.5))
```



```{r}
cor(iris$Petal.Length, iris$Petal.Width)
```


```{r}
a <- 1:12
x <- matrix(a, 2, 3)
```


```{r}
x <- seq(1,10,0.5)
summary(x)
```























