#install.packages("ROCR")
library(ROCR)
library(e1071)

credit_fraud=read.csv("../data/qconlondon2016_sample_data.csv")
head(credit_fraud)

credit_fraud$charge_time=as.numeric(credit_fraud$charge_time)

set.seed(1984)
T = sample(1:nrow(credit_fraud),round(0.3*nrow(credit_fraud)))  

credit_fraud_test=credit_fraud[T,]
credit_fraud_train=credit_fraud[-T,]

fit = naiveBayes(fraudulent~.,data=credit_fraud_train,laplace=1)

names(fit)
fit$apriori
fit$tables

predict_test = predict(fit, newdata=credit_fraud_test)

c_matrix=table(credit_fraud_test$fraudulent,predict_test)
print(c_matrix)

cat('Accuracy: ', sum(diag(c_matrix))/sum(c_matrix)*100, ' %')

# ROC curve
pr=prediction(as.numeric(predict_test),as.numeric(credit_fraud_test$fraudulent)) 
prf=performance(pr, measure="tpr", x.measure="fpr")
plot(prf,colorize=TRUE)

auc=performance(pr, measure="auc")
auc=auc@y.values[[1]]
auc


