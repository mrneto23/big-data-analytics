#
# Atividade de Aprofundamento 
# Tarefa 2 = Logit Cross Validation
#
# Altere o c?digo abaixo para obter avaliar a acuracidade do modelo log?stico
# emprega parti??es de teste que tenham apenas 1 elemento.
#
# Em seguida responda a quest?o 3 e 4 do question?rio. 
#
# N?O ? NECESS?RIO POSTAR ESSE C?DIGO
#
install.packages("ROCR")
install.packages("dummies")
install.packages("caret") 
install.packages("e1071") 
library(ROCR)
library(dummies)
library(caret) # for Cross Validation functions
library(e1071)

biopsy_ = na.omit(biopsy[,-c(1)]) # 1 = ID

# Altere para num?rico 0 ou 1
biopsy_$class = ifelse(biopsy_$class == 'benign', 1, 0)

# Altere o valor de number= para que as parti??es de teste que tenham apenas 1 elemento  
ctrl <- trainControl(method="repeatedcv", number= nrow(biopsy_), repeats=1)

fit <- train(class~., data=biopsy_,
                  method="glm", 
                  family="binomial",
                  trControl=ctrl, 
                  preProcess = c("center", "scale"))
fit

predict_test = predict(fit, newdata=biopsy_, type="raw")

# predict_test contem valores 0-1 com a probabilidade de "malignant" ou "benign"
# altere predict_test para um vetor contendo os valores "malignant","benign"
#
# sugest?o: empregue elseif para atribuir 1="malignant" ou 0="benign" (valores num?ricos) conforme o 
# valor seja >0.5
#
predict_test = ____________

# Gere a matrix de confus?o 
c_matrix = __________
print(c_matrix)

# Calcule a acuracidade
cat('Accuracy: ', sum(diag(c_matrix))/sum(c_matrix)*100, ' %', "\n")

# ROC curve 
pr=prediction(predict_test,biopsy_$class) 
prf=performance(pr, measure="tpr", x.measure="fpr")
plot(prf,colorize=TRUE)

# Area under ROC curve 
auc=performance(pr, measure="auc")
auc=auc@y.values[[1]]
auc
