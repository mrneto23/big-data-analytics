---
title: "Regressao logistica"
author: "Moacyr Rodrigues Neto"
date: "March 31, 2019"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r}
library(ROCR)
library(readr)
```

```{r}
credit_fraud <- read.csv("../data/qconlondon2016_sample_data.csv")
head(credit_fraud)
```
## Fixando a semente de geração aleatório para trazer sempre o mesmo resultado
```{r}
set.seed(1984)
```

## Separando a base de teste e de treinamento
```{r}
t <- sample(1:nrow(credit_fraud), round(0.3 * nrow(credit_fraud)))
credit_fraud_test <- credit_fraud[t,]
credit_fraud_train <- credit_fraud[-t,]
```

## Convertendo a coluna charge_time em númerica
```{r}
credit_fraud_test$charge_time <- as.numeric(credit_fraud_test$charge_time)
credit_fraud_train$charge_time <- as.numeric(credit_fraud_train$charge_time)
```

##
```{r}
table(credit_fraud$card_country)
```

## Aplicando o modelo
```{r}
fit <- glm(fraudulent~., data = credit_fraud_train, family = binomial("logit"))
```

## Predição 
```{r}
predict_test <- predict(fit, newdata = credit_fraud_test, type = "response") > 0.5
```

## Matriz de confusão
```{r}
c_matrix <- table(credit_fraud_test$fraudulent, predict_test)
print(c_matrix)
```

## Acuracidade
```{r}
cat('Accuracy: ', sum(diag(c_matrix)) / sum(c_matrix) * 100, ' %')
```


## Explorando o modelo 
```{r}
exp(coef(fit))
```

```{r}
summary(fit)
```


## Curva ROC
```{r}
pr <- prediction(as.numeric(predict_test), as.numeric(credit_fraud_test$fraudulent))
prf <- performance(pr, measure = "tpr", x.measure = "fpr")
plot(prf, colorize = TRUE)
```

```{r}
auc <- performance(pr, measure = "auc")
auc <- auc@y.values[[1]]
auc
```





















