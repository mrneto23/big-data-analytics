---
title: "A2 - Biopsy e Credit Approval Data Set"
author: "Moacyr Rodrigues Neto"
date: "April 3, 2019"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```


# Tarefa 1

Atividade de Aprofundamento 
Tarefa 1 = Knn Cross Validation

Altere o código abaixo que avalia modelos knn para k=3,4 e com 5 partições
para avaliar modelos knn com k=1:12 e para que as partições de teste que tenham
apenas 1 elemento.

Em seguida responda a questão 1 e 2 do questionário.

NÃO É NECESSÁRIO POSTAR ESSE CÓDIGO

```{r}
library(MASS)
library(class)
library(caret) # for Cross Validation functions
library(e1071)
```


Problemas com a carga das libraries? Execute antes
 
install.packages(" nome da library ") 

não esqueça das aspas

```{r}
biopsy_ = na.omit(biopsy[,-c(1)]) # 1 = ID é eliminado assim como os valores NA  

ctrl <- trainControl(method="repeatedcv", number=nrow(biopsy_), repeats=1)
nn_grid <- expand.grid(k=c(1:12))
nn_grid

best_knn <- train(class~., data=biopsy_,
                  method="knn",
                  trControl=ctrl, 
                  preProcess = c("center", "scale"),  # standardize
                  tuneGrid=nn_grid)

print(best_knn) 
```

# Tarefa 2

Atividade de Aprofundamento 
Tarefa 2 = Logit Cross Validation

Altere o código abaixo para obter avaliar a acuracidade do modelo logístico
emprega partições de teste que tenham apenas 1 elemento.

Em seguida responda a questão 3 e 4 do questionário. 

NÃO É NECESSÁRIO POSTAR ESSE CÓDIGO

```{r}
#install.packages("ROCR")
#install.packages("dummies")
#install.packages("caret") 
#install.packages("e1071") 
library(ROCR)
library(dummies)
library(caret) # for Cross Validation functions
library(e1071)

```

```{r}
biopsy_ = na.omit(biopsy[,-c(1)]) # 1 = ID
```


Altere para numérico 0 ou 1
```{r}
biopsy_$class <- ifelse(biopsy_$class == 'benign',0,1) 
```



Altere o valor de number= para que as particoes de teste que tenham apenas 1 elemento  
```{r}
ctrl <- trainControl(method="repeatedcv", number=nrow(biopsy_), repeats=1)

fit <- train(class~., data=biopsy_,
                  method="glm", 
                  family="binomial",
                  trControl=ctrl, 
                  preProcess = c("center", "scale"))
fit



predict_test = predict(fit, newdata=biopsy_, type="raw")

```


predict_test contem valores 0-1 com a probabilidade de "malignant" ou "benign"
altere predict_test para um vetor contendo os valores "malignant","benign"

sugestão: empregue elseif para atribuir 1="malignant" ou 0="benign" (valores numéricos) conforme o 
valor seja >0.5

```{r}
#predict_test = ifelse(predict_test > 0.5, "malignant", "benign")
```

Gere a matrix de confusão
```{r}
c_matrix = table(biopsy_$class, predict_test)
print(c_matrix)
```

Calcule a acuracidade
```{r}
cat('Accuracy: ', sum(diag(c_matrix))/sum(c_matrix)*100, ' %', "\n")
```


ROC curve 
```{r}
pr=prediction(predict_test,biopsy_$class) 
prf=performance(pr, measure="tpr", x.measure="fpr")
plot(prf,colorize=TRUE)
```


Area under ROC curve 
```{r}
auc=performance(pr, measure="auc")
auc=auc@y.values[[1]]
auc
```















