---
title: "Estatística"
author: "Moacyr Rodrigues Neto"
date: "2 de janeiro de 2019"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# Conceitos básicos de probabilidade

## Amostragem aleatória

## Experimento aleatório ou fenômeno aleatório

## Operações com eventos

## Operações sobre conjuntos

## Amostragem aleatória - exemplos no R

função sample - mais informações em help(sample)

### Amostragem sem substituição (valores da amostra não se repetem)

Seleciona 5 números ao acaso de um conjunto de 1 até 40
```{r}
# x = 1:40
# size = 5
sample(1:40, 5)
```

Se informar um único número, sera interpretado como o tamanho da sequência
```{r}
sample(40, 5)
```

### Amostragem com substituição (valores da amostra podem repetir)

```{r}
# x = 1:40
# size = 5
sample(1:40, 5, replace = TRUE)
```

### Simular lançamento de moedas

```{r}
# 10 lançamentos de moeda
sample(c("Cara", "Coroa"), 10, replace = TRUE)
```

### Simular lançamento de dados

```{r}
# 10 lançamentos do dado
sample(1:6, size = 10, replace = TRUE)
```

### Simular dados com probabilidades não iguais
Ex: 90% de chance de sucesso

```{r}
sample(c("Sucesso", "Falha"), 10, replace = T, prob = c(0.9, 0.1))
```

### Gerar amostra

Se sabemos que uma população de 10000 pessoas 6800 que responderam "sim" á nossa pesquisa.

Amostra de 10 pessoas

```{r}
sample(rep(c("não", "sim"), c(3200, 6800)), size = 10, replace = T)
```

A função rep() produz 10000 valores: 3200 "não"s e 6800 "sim"s
  Um teste com 100 valores
  
```{r}
rep(c("não", "sim"), c(32, 68))
```
  
## Probabilidade clássica ou a priori

## Probabilidade frequentista ou a posteriori

```{r}
# Moeda balanceada 50% de chance
p0 <- 1/2

# Intervalo entre 100 e 5000
n = 100:5000

# Frequencia
fr = mapply(function(x) sum(rbinom(x, 1, p0)) / x, n)

# Gráfico
plot(n, fr, ylab = "Frequencia relativa", type = "l") %>% 
  abline(h = p0, lty = 2, col = "blue")
```

## Cálculos de probabilidade e combinatória
Vamos retornar ao exemplo da amostragem sem substitiução (sample(1:40,5))
  A probabilidade de se obter um dado número como o primeiro da amostra deve ser 1/40, o próximo 1/39, e assim por diante;
  a probabilidade de uma dada amostra deve ser então 1/(40x39x38x37x36);
  no R, usamos a função prod() que calcula o produto de um vetor de números.

```{r}
1/prod(40:36)
```


Mas se for como uma loteria, então estamos interessados na probabilidade de se obter um conjunto de números corretamente.
Assim, o que precisamos fazer é incluir os casos que dão os mesmos números em uma ordem diferente.

Como a probabilidade de cada um desses casos será a mesma, tudo o que temos que fazer é achar quantos destes casos existem e multiplicar por esta quantidade.

  existem cinco possibilidades para o primeiro número, e para cada um deles haverá quatro possibilidades para o segundo, tres para o terceiro e assim por diante;
  isso é, o número é 5x4x3x2x1;
  este número é escrito tambel como 5! (5 fatorial).
  
```{r}
prod(5:1) / prod(40:36)
```


Existe uma outra maneira de se chegar ao mesmo resultado.
Primeiro, observamos que o conjunto real de números é irrelevante; todos os conjuntos de cinco números devem ter a mesma probabilidade.
De forma genérica, seria o número de maneiras que x objetos (subconjuntos) podem ser escolhidos de n objetos distintos; 
lê-se “n choose x”. 

```{r}
1 / choose(40, 5)
```

## Definição de probabilidade












## 

dnorm - densidade normal
pnorm - probabilidade normal 
qnorm - quantis normal
rnorm - random normal

### Densidades

A densidade para uma distribuição contínua é a medida da probabilidade relativa de se conseguir um valor próximo a x.
A probabilidade de se obter um valor em um intervalo particular é a área sob a parte correspondente da curva.
Para distribuições discretas, utilizamos o termo probabilidade pontual ao invés de densidade, pois temos a probabilidade de
obter exatamente o valor de x.
Uma função de densidade é apresentada a seguir. Apesar de ser um dos quatro tipos que é menos utilizado na prática, mas
se queremos plotar a tão conhecida curva sino da distribuição normal, podemos fazer assim:



```{r}
x <- seq(-4, 4, 0.1)
plot(x, dnorm(x), type = "l")
```

Uma maneira alternativa de se criar o mesmo gráfico é utilizar a função curve como segue:


```{r}
curve(dnorm(x), from = -4, to = 4)
```


Para distribuições discretas, onde as variáveis podem assumir apenas alguns valores distintos, é preferível fazer um diagrama de
“pinos”. Aqui um exemplo para a distribuição binomial com n = 50 e p = 0.33.

```{r}
x <- 0:50
plot(x, dbinom(x, size = 50, prob = 0.33), type = "h")
```

Para a função dbinom precisamos especificar três argumentos.
Além do x, temos que especificar o número de tentativas, n e o parâmetro de probabilidade p.
A distribuição plotada corresponde, por exemplo, ao número de 5s ou 6s em 50 lançamentos de um dado simétrico.
Na realidade, dnorm também pode receber mais de um argumento, a saber, a média e o desvio padrão, mas eles têm valor padrão de 0
e 1, já que é o mais utilizado para a distribuição normal.

### Funções de distribuição cumulativas

A função de distribuição cumulativa descreve a probabilidade de atingir x ou menos em uma dada distribuição.
As funções do R correspondentes começam com p (probabilidade) por convenção.
Assim como para as densidades, podemos também plotar uma função de distribuição cumulativa, mas não é muito informativa.
Mais frequentemente, estamos interessados em números!
Digamos que seja conhecido que alguma medida biomédica em indivíduos saudáveis seja bem descrita por uma distribuição
normal com uma média de 132 e um desvio padrão de 13.

Então, se um paciente tem um valor de 160, vamos calcular a probabilidade de sua ocorrência na população:

```{r}
1 - pnorm(160, mean = 132, sd = 13)
```


Ou seja, há somente cerca de 1.5% da população em geral que tenha este valor ou maior.
A função pnorm retorna a probabilidade de se obter um valor menor do que o seu primeiro argumento em uma distribuição
normal com a dada média e desvio padrão.


### Quantis

A função quantile é o inverso da função de distribuição acumulada.
O p-quantil é o valor com a propriedade de que há uma probabilidade p de se obter um valor menor ou igual a ele.

A mediana é por definição o quantil 50%.

Se temos n observações distribuídas normalmente com a mesma média μ e desvio padrão σ, então sabemos que a média (average)
(Xbarra) é normalmente distribuída em torno de μ com um desvio padrão σ/√n.

Um intervalo de confiança de 95% para a μ pode ser obtido como:

```{r}
xbar <- 83
sigma <- 12
n <- 5
(sem <- sigma/sqrt(n))
xbar + sem * qnorm(0.025)
xbar + sem * qnorm(0.975)
```



## Inferência estatistica

### Estimação pontual

#### distribuição binominal
#### distribuição de Poisson
#### distribuição exponencial
#### distribuição normal

### Estimação intervalr (conjunto de valores)

### Teste de hipóteses
#### Tipos de erros

Quando estamos fazendo um teste de hipótese estamos sujeitos a dois tipos de erro:

   erro tipo I - rejeitar H0 quando H0 é verdadeira;
   erro tipo II - não rejeitar (aceitar) H0 quando H0 é falsa.

Define-se um limite superior para a probabilidade máxima de erro tipo I que deve ser tolerada. Este limite é o nível de significância,
denotado por convenção como α. 

O nível de significância é especificado (antes de se examinar os dados) e somente consideram-se as regras de decisão para as quais a probabilidade
de erro tipo I não seja maior do que α.

O nível de significância especifica o quão pequena deve ser a probabilidade de significância para se concluir que um fenômeno
não é uma coincidência. Os valores típicos de níveis de significância são: α = 0.05 e α = 0.01.

Assim, tomando p como a probabilidade de significância e α o nível de significância, consideramos a regra que a hipótese
nula deve ser rejeitada se e somente se p ≤ α.

#### Hipóteses bilaterais e unilaterais

Sejam as hipóteses nula e alternativa:

• H0 : μ = μ0;
• H1 : μ ≠ μ0;

em que μ_0 é uma constante conhecida (valor de teste), o teste é chamado de bilateral.

Podemos ter também as hipóteses:

• H0 : μ = μ0;
• H1 : μ < μ0. ← unilateral à esquerda;

ou

• H0 : μ = μ0;
• H1 : μ > μ0. ← unilateral à direita.

É interessante expressar H0 em forma de igualdade, ou seja, fazer teste bilateral, mas as outras expressões são também aceitáveis.

Retomando o exemplo do lançamento de uma moeda, consideramos a possibilidade de a moeda ser justa (p = 0.5) contra a possibilidade de não ser justa (p ≠ 0.5). 
Em um experimento para verificar estas possibilidades, Arlen (nossa personagem fictícia) faz o lançamento da moeda 100 vezes e observa o resultado. 
Em 68 vezes ela obteve coroa (tails) contra 32 caras (heads).

```{r}
p <- pbinom(32, 100, 0.5) + 1 - pbinom(67, 100, 0.5)
p
```









```{r}
xbar <- 62
mu0 <- 60
Var <- 25
sigma <- sqrt(Var)
alpha <- 0.05
n <- 16
z <- (xbar - mu0) / (sigma / sqrt(n))
p <- 2 * pnorm(-abs(z))
p

```

#### Estimação intervalar

```{r}
lim_inf <- mu0 - qnorm(1 - alpha/2) * sigma / sqrt(n)
lim_inf
lim_sup <- mu0 + qnorm(1 - alpha/2) * sigma / sqrt(n)
lim_sup
```


Pagina 58 T2















